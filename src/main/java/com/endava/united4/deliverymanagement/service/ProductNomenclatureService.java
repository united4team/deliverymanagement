package com.endava.united4.deliverymanagement.service;

import com.endava.united4.deliverymanagement.entities.ProductNomenclature;
import com.endava.united4.deliverymanagement.exceptions.ApplicationException;
import com.endava.united4.deliverymanagement.repositories.abstractrep.ProductNomenclatureRepository;
import com.endava.united4.deliverymanagement.utils.ExceptionType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductNomenclatureService {

    private final ProductNomenclatureRepository productNomenclatureRepository;

    public ProductNomenclature saveProduct(ProductNomenclature productNomenclature) {
        return productNomenclatureRepository.save(productNomenclature);
    }

    public List<ProductNomenclature> selectAll() {
        return productNomenclatureRepository.findAll();
    }

    public ProductNomenclature updateProduct(ProductNomenclature productNomenclature, String uuid) {

        ProductNomenclature updatable = productNomenclatureRepository.findById(uuid).orElseThrow(() -> new ApplicationException(ExceptionType.PRODUCT_NOT_FOUND));
        updatable.setProductName(productNomenclature.getProductName());
        updatable.setProductWeight(productNomenclature.getProductWeight());
        updatable.setPrice(productNomenclature.getPrice());
        updatable.setExpDuration(productNomenclature.getExpDuration());
        return productNomenclatureRepository.save(updatable);
    }

    public ProductNomenclature deleteProduct(String uuid) {
        ProductNomenclature deleted = productNomenclatureRepository.findById(uuid).orElseThrow(() -> new ApplicationException(ExceptionType.PRODUCT_NOT_FOUND));
        try {
            productNomenclatureRepository.deleteById(uuid);
        } catch (Exception e) {
            throw new ApplicationException(ExceptionType.NOMENCLATURE_IN_USE);
        }

        return deleted;
    }

    public ProductNomenclature findByProductUuid(String uuid) {
        return productNomenclatureRepository.findById(uuid).orElseThrow(() -> new ApplicationException(ExceptionType.PRODUCT_NOT_FOUND));
    }
}
