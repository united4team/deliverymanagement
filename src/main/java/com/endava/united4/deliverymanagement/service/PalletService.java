package com.endava.united4.deliverymanagement.service;

import com.endava.united4.deliverymanagement.entities.Pallet;
import com.endava.united4.deliverymanagement.entities.Product;
import com.endava.united4.deliverymanagement.exceptions.ApplicationException;
import com.endava.united4.deliverymanagement.repositories.abstractrep.PalletRepository;
import com.endava.united4.deliverymanagement.repositories.abstractrep.ProductRepository;
import com.endava.united4.deliverymanagement.utils.ExceptionType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class PalletService {

    private final PalletRepository palletRepository;
    private final ProductRepository productRepository;

    public Pallet savePallet(Pallet pallet) {
        return palletRepository.save(pallet);
    }

    public List<Pallet> selectAll() {
        return palletRepository.findAll();
    }

    public Pallet updatePallet(Pallet pallet, String palletUUID) {
        Pallet updatable = palletRepository.findById(palletUUID).orElseThrow(() -> new ApplicationException(ExceptionType.PALETTE_NOT_FOUND));
        updatable.setDimensionHeight(pallet.getDimensionHeight());
        updatable.setDimensionLength(pallet.getDimensionLength());
        updatable.setDimensionWidth(pallet.getDimensionWidth());
        updatable.setPalletWeight(pallet.getPalletWeight());
        return palletRepository.save(pallet);
    }

    public Pallet deletePallet(String palletUUID) {
        Pallet deleted = palletRepository.findById(palletUUID).orElseThrow(() -> new ApplicationException(ExceptionType.PALETTE_NOT_FOUND));
        try {
            palletRepository.deleteById(palletUUID);
        } catch (Exception e) {
            throw new ApplicationException(ExceptionType.PALETTE_NOT_EMPTY);
        }
        return deleted;
    }

    public Pallet findByUUID(String palletUUID) {
        return palletRepository.findById(palletUUID).orElseThrow(() -> new ApplicationException(ExceptionType.PALETTE_NOT_FOUND));
    }

    public List<Product> findAllPalletProducts(String palletUUID) {
        return productRepository.findAllByPalletPalletUUID(palletUUID);
    }

    public Product deleteProductFromPallet(String productUuId) {
        Product deleted = productRepository.findById(productUuId).orElseThrow(() -> new ApplicationException(ExceptionType.PRODUCT_NOT_FOUND));
        deleted.setPallet(null);
        return productRepository.save(deleted);
    }

    public Product updateProductOnPallet(String palletUUID, String removedProductUuid, String addedProductUuid) {
        Pallet pallet = palletRepository.findById(palletUUID).orElseThrow(() -> new ApplicationException(ExceptionType.PALETTE_NOT_FOUND));
        Product added = productRepository.findById(addedProductUuid).orElseThrow(() -> new ApplicationException(ExceptionType.PRODUCT_NOT_FOUND));
        Product removed = productRepository.findById(removedProductUuid).orElseThrow(() -> new ApplicationException(ExceptionType.PRODUCT_NOT_FOUND));
        removed.setPallet(null);
        added.setPallet(pallet);
        productRepository.save(removed);
        return productRepository.save(added);
    }

    public List<Product> deleteAllFromPallet(String palletUuid) {
        List<Product> products = productRepository.findAllByPalletPalletUUID(palletUuid);
        products.forEach(product -> product.setPallet(null));
        products.forEach(productRepository::save);
        return productRepository.findAllByPalletPalletUUID(palletUuid);
    }

    public Product addProductOnPallet(String palletUuid, String productUuid) {
        Product added = productRepository.findById(productUuid).orElseThrow(() -> new ApplicationException(ExceptionType.PRODUCT_NOT_FOUND));
        Pallet pallet = palletRepository.findById(palletUuid).orElseThrow(() -> new ApplicationException(ExceptionType.PALETTE_NOT_FOUND));
        added.setPallet(pallet);
        return productRepository.save(added);
    }


}
