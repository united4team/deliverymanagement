package com.endava.united4.deliverymanagement.service;

import com.endava.united4.deliverymanagement.entities.Product;
import com.endava.united4.deliverymanagement.entities.ProductNomenclature;
import com.endava.united4.deliverymanagement.exceptions.ApplicationException;
import com.endava.united4.deliverymanagement.repositories.abstractrep.ProductNomenclatureRepository;
import com.endava.united4.deliverymanagement.repositories.abstractrep.ProductRepository;
import com.endava.united4.deliverymanagement.utils.ExceptionType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final ProductNomenclatureRepository productNomenclatureRepository;

    public Product saveProduct(String nomenclatureUuid, Product product) {
        Product savedProduct = product;
        ProductNomenclature nomenclature = productNomenclatureRepository.findById(nomenclatureUuid).orElseThrow(() -> new ApplicationException(ExceptionType.PRODUCT_NOT_FOUND));
        savedProduct.setNomenclature(nomenclature);
        return productRepository.save(savedProduct);
    }

    public List<Product> selectAll() {
        return productRepository.findAll();
    }

    public Product updateProduct(Product product, String uuid) {
        Product updatable = productRepository.findById(uuid).orElseThrow(()->new ApplicationException(ExceptionType.PRODUCT_NOT_FOUND));
        updatable.setProductionDate(product.getProductionDate());
        return productRepository.save(updatable);
    }

    public Product deleteProduct(String uuid) {
        Product deleted =productRepository.findById(uuid).orElseThrow(()->new ApplicationException(ExceptionType.PRODUCT_NOT_FOUND));
        productRepository.deleteById(uuid);
        return deleted;
    }

    public Product findByProductUuid(String uuid) {
        return productRepository.findById(uuid)
                .orElseThrow(() -> new ApplicationException(ExceptionType.PRODUCT_NOT_FOUND));
    }
}
