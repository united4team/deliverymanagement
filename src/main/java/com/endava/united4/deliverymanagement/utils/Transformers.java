package com.endava.united4.deliverymanagement.utils;

import com.endava.united4.deliverymanagement.dto.*;
import com.endava.united4.deliverymanagement.entities.Pallet;
import com.endava.united4.deliverymanagement.entities.Product;
import com.endava.united4.deliverymanagement.entities.ProductNomenclature;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.UUID;
import java.util.function.Function;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Transformers {

    public static final Function<ProductNomenclatureRequestDto, ProductNomenclature> convertFromRequestDtoToNomenclature =
            productNomenclatureRequestDto -> ProductNomenclature.builder()
                    .productName(productNomenclatureRequestDto.getProductName())
                    .productWeight(productNomenclatureRequestDto.getProductWeight())
                    .price(productNomenclatureRequestDto.getPrice())
                    .expDuration(productNomenclatureRequestDto.getExpDuration())
                    .nomenclatureUuid(UUID.randomUUID().toString())
                    .build();

    public static final Function<ProductNomenclature, ProductNomenclatureResponseDto> convertFromNomenclatureToDto =
            product -> ProductNomenclatureResponseDto.builder()
                    .productName(product.getProductName())
                    .productWeight(product.getProductWeight())
                    .price(product.getPrice())
                    .expDuration(product.getExpDuration())
                    .productUuId(product.getNomenclatureUuid())
                    .build();


    public static final Function<PalletRequestDto, Pallet> convertFromRequestDtoToPallet =
            palletRequestDto -> Pallet.builder()
                    .palletWeight(palletRequestDto.getPalletWeight())
                    .dimensionWidth(palletRequestDto.getDimensionWidth())
                    .dimensionHeight(palletRequestDto.getDimensionHeight())
                    .dimensionLength(palletRequestDto.getDimensionLength())
                    .palletUUID(UUID.randomUUID().toString())
                    .build();

    public static final Function<Pallet, PalletResponseDto> convertFromPalletToDto =
            pallet -> PalletResponseDto.builder()
                    .palletWeight(pallet.getPalletWeight())
                    .dimensionWidth(pallet.getDimensionWidth())
                    .dimensionHeight(pallet.getDimensionHeight())
                    .dimensionLength(pallet.getDimensionLength())
                    .palletUUID(pallet.getPalletUUID())
                    .build();

    public static final Function<Product, ProductResponseDto> convertFromProductToDto =
            product -> ProductResponseDto.builder()
                    .productName(product.getNomenclature().getProductName())
                    .productUuid(product.getProductUuid())
                    .productionDate(product.getProductionDate())
                    .build();

    public static final Function<ProductRequestDto, Product> convertFromRequestDtoToProduct =
            productRequestDto -> Product.builder()
                    .productionDate(productRequestDto.getProductionDate())
                    .productUuid(UUID.randomUUID().toString())
                    .build();
}
