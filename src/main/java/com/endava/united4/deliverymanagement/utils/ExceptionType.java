package com.endava.united4.deliverymanagement.utils;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Getter
@RequiredArgsConstructor
public enum ExceptionType {
    CLIENT_ERROR("bad request", BAD_REQUEST),
    RESOURCE_NOT_FOUND_BY_NAME("Resource not found by name", NOT_FOUND),
    PRODUCT_NOT_FOUND("Product not found", NOT_FOUND),
    PALETTE_NOT_FOUND("Palette not found", NOT_FOUND),
    PALETTE_NOT_EMPTY("Palette is not empty, please remove all products", BAD_REQUEST),
    NOMENCLATURE_IN_USE("There are products of this nomenclature, please delete all products", BAD_REQUEST);

    private final String message;
    private final HttpStatus statusCode;
}
