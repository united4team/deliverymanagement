package com.endava.united4.deliverymanagement.facade;

import com.endava.united4.deliverymanagement.dto.PalletRequestDto;
import com.endava.united4.deliverymanagement.dto.PalletResponseDto;
import com.endava.united4.deliverymanagement.dto.ProductResponseDto;
import com.endava.united4.deliverymanagement.entities.Pallet;
import com.endava.united4.deliverymanagement.entities.Product;
import com.endava.united4.deliverymanagement.service.PalletService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static com.endava.united4.deliverymanagement.utils.Transformers.*;

@Component
@RequiredArgsConstructor
public class PalletFacade {

    private final PalletService palletService;

    public Pallet createPallet(PalletRequestDto palletRequestDto) {
        return palletService.savePallet(convertFromRequestDtoToPallet.apply(palletRequestDto));
    }

    public List<PalletResponseDto> findAll() {
        return palletService.selectAll().stream()
                .map(convertFromPalletToDto)
                .collect(Collectors.toList());
    }

    public PalletResponseDto updatePallet(PalletRequestDto palleteRequestDto, String palletUUID) {
        val updatedPalette = palletService
                .updatePallet(convertFromRequestDtoToPallet.apply(palleteRequestDto), palletUUID);

        return convertFromPalletToDto.apply(updatedPalette);
    }

    public PalletResponseDto deletePallet(String palletUUID) {
        return convertFromPalletToDto.apply(palletService.deletePallet(palletUUID));
    }

    public PalletResponseDto findByUUID(String palletUUID) {
        val palette = palletService.findByUUID(palletUUID);
        return convertFromPalletToDto.apply(palette);
    }

    public List<ProductResponseDto> findAllPalletProducts(String palletUUID) {
        List<Product> products = palletService.findAllPalletProducts(palletUUID);
        return products
                .stream()
                .map(convertFromProductToDto)
                .collect(Collectors.toList());
    }

    public ProductResponseDto updateProductOnPallet(String palletUuid, String removedProductUuid, String addedProductUuid) {
        return convertFromProductToDto.apply(palletService.updateProductOnPallet(palletUuid, removedProductUuid, addedProductUuid));
    }

    public ProductResponseDto deleteProductFromPallet(String productUuid) {
        return convertFromProductToDto.apply(palletService.deleteProductFromPallet(productUuid));
    }

    public List<ProductResponseDto> deleteAllFromPallet(String palletUuid) {
        return palletService.deleteAllFromPallet(palletUuid)
                .stream()
                .map(convertFromProductToDto)
                .collect(Collectors.toList());
    }

    public ProductResponseDto addProductOnPallet(String palletUuid, String productUuid) {
        return convertFromProductToDto.apply(palletService.addProductOnPallet(palletUuid, productUuid));
    }
}

