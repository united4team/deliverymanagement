package com.endava.united4.deliverymanagement.facade;

import com.endava.united4.deliverymanagement.dto.ProductRequestDto;
import com.endava.united4.deliverymanagement.dto.ProductResponseDto;
import com.endava.united4.deliverymanagement.service.ProductService;
import com.endava.united4.deliverymanagement.utils.Transformers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static com.endava.united4.deliverymanagement.utils.Transformers.convertFromProductToDto;
import static com.endava.united4.deliverymanagement.utils.Transformers.convertFromRequestDtoToProduct;

@Component
@RequiredArgsConstructor
public class ProductFacade {

    private final ProductService productService;

    public ProductResponseDto saveProduct(ProductRequestDto productRequestDto, String nomenclatureUuid) {
        return convertFromProductToDto.apply(productService.saveProduct(nomenclatureUuid, convertFromRequestDtoToProduct.apply(productRequestDto)));
    }

    public List<ProductResponseDto> findAll() {
        return productService.selectAll()
                .stream()
                .map(convertFromProductToDto)
                .collect(Collectors.toList());
    }

    public ProductResponseDto updateProduct(String productUuid, ProductRequestDto productRequestDto) {
        return convertFromProductToDto.apply(productService.updateProduct(convertFromRequestDtoToProduct.apply(productRequestDto), productUuid));
    }


    public ProductResponseDto deleteProduct(String productUuid) {
        return convertFromProductToDto.apply(productService.deleteProduct(productUuid));
    }

    public ProductResponseDto findProductByUuid(String productUuid) {
        return convertFromProductToDto.apply(productService.findByProductUuid(productUuid));
    }

    public ProductResponseDto createProduct(String nomenclatureUuid, ProductRequestDto productRequestDto) {
        return Transformers.convertFromProductToDto
                .apply(productService.saveProduct(nomenclatureUuid, Transformers.convertFromRequestDtoToProduct
                        .apply(productRequestDto)));
    }
}
