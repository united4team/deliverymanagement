package com.endava.united4.deliverymanagement.facade;

import com.endava.united4.deliverymanagement.dto.ProductNomenclatureRequestDto;
import com.endava.united4.deliverymanagement.dto.ProductNomenclatureResponseDto;
import com.endava.united4.deliverymanagement.service.ProductNomenclatureService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static com.endava.united4.deliverymanagement.utils.Transformers.convertFromNomenclatureToDto;
import static com.endava.united4.deliverymanagement.utils.Transformers.convertFromRequestDtoToNomenclature;

@Component
@RequiredArgsConstructor
public class NomenclatureFacade {

    private final ProductNomenclatureService productNomenclatureService;

    public ProductNomenclatureResponseDto saveProduct(ProductNomenclatureRequestDto productNomenclatureRequestDto) {

        return convertFromNomenclatureToDto.apply(productNomenclatureService.saveProduct(convertFromRequestDtoToNomenclature.apply(productNomenclatureRequestDto)));
    }

    public List<ProductNomenclatureResponseDto> findAll() {
        return productNomenclatureService
                .selectAll()
                .stream()
                .map(convertFromNomenclatureToDto)
                .collect(Collectors.toList());
    }

    public ProductNomenclatureResponseDto updateProduct(ProductNomenclatureRequestDto productNomenclatureRequestDto, String uuid) {
        val updatedProduct = productNomenclatureService
                .updateProduct(convertFromRequestDtoToNomenclature.apply(productNomenclatureRequestDto), uuid);
        return convertFromNomenclatureToDto.apply(updatedProduct);
    }

    public ProductNomenclatureResponseDto deleteProduct(String uuid) {
        return convertFromNomenclatureToDto.apply(productNomenclatureService.deleteProduct(uuid));
    }

    public ProductNomenclatureResponseDto findByProductUuid(String uuid) {
        val product = productNomenclatureService.findByProductUuid(uuid);
        return convertFromNomenclatureToDto.apply(product);
    }
}
