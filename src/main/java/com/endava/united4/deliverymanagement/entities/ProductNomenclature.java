package com.endava.united4.deliverymanagement.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "PRODUCT_NOMENCLATURE")
public class ProductNomenclature implements Serializable {

    @Column(name = "product_name")
    private String productName;

    @Column(name = "weight")
    private Double productWeight;

    @Column(name = "price")
    private Double price;

    @Column(name = "exp_duration")
    private Integer expDuration;

    @Id
    @Column(name = "nomenclature_uuid")
    private String nomenclatureUuid;
}
