package com.endava.united4.deliverymanagement.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity(name = "Products")
@Table
public class Product {

    @Id
    @Column(name = "product_uuid")
    private String productUuid;

    @ManyToOne
    @JoinColumn(name = "nomenclature_uuid", referencedColumnName = "nomenclature_uuid")
    private ProductNomenclature nomenclature;

    @Column(name = "production_date")
    private LocalDate productionDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pallet_uuid", referencedColumnName = "pallet_uuid")
    private Pallet pallet;

}
