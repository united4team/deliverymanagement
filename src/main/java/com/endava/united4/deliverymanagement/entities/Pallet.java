package com.endava.united4.deliverymanagement.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "pallets")
public class Pallet implements Serializable {

    @Column(name = "pallet_weight")
    private Double palletWeight;

    @Column(name = "dimension_width")
    private Double dimensionWidth;

    @Column(name = "dimension_height")
    private Double dimensionHeight;

    @Column(name = "dimension_length")
    private Double dimensionLength;

    @Id
    @Column(name = "pallet_uuid")
    private String palletUUID;
}
