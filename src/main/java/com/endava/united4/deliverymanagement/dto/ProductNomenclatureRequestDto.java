package com.endava.united4.deliverymanagement.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductNomenclatureRequestDto {
    private final String productName;
    private final Double productWeight;
    private final Double price;
    private final Integer expDuration;
}
