package com.endava.united4.deliverymanagement.dto;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@Builder
public class PalletRequestDto {
   private final Double palletWeight;
   private final Double dimensionWidth;
   private final Double dimensionHeight;
   private final Double dimensionLength;
}
