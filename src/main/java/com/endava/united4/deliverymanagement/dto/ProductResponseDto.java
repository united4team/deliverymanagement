package com.endava.united4.deliverymanagement.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class ProductResponseDto {
    private final String productName;
    private final String productUuid;
    private final LocalDate productionDate;
}
