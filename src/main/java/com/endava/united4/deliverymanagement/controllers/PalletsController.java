package com.endava.united4.deliverymanagement.controllers;


import com.endava.united4.deliverymanagement.dto.PalletRequestDto;
import com.endava.united4.deliverymanagement.dto.PalletResponseDto;
import com.endava.united4.deliverymanagement.dto.ProductResponseDto;
import com.endava.united4.deliverymanagement.entities.Pallet;
import com.endava.united4.deliverymanagement.facade.PalletFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pallets")
@RequiredArgsConstructor
public class PalletsController {

    private final PalletFacade palletFacade;

    @PostMapping
    public Pallet createPallet(@RequestBody PalletRequestDto palletRequestDto) {
        return palletFacade.createPallet(palletRequestDto);
    }

    @GetMapping
    public List<PalletResponseDto> getAllPallets() {
        return palletFacade.findAll();
    }

    @GetMapping("/{palletUUID}")
    public PalletResponseDto findByUUID(@PathVariable String palletUUID) {
        return palletFacade.findByUUID(palletUUID);
    }

    @PutMapping("/{palletUUID}")
    public PalletResponseDto updatePallet(@RequestBody PalletRequestDto palletRequestDto, @PathVariable String palletUUID) {
        return palletFacade.updatePallet(palletRequestDto, palletUUID);
    }

    @DeleteMapping("/{palletUUID}")
    public PalletResponseDto deletePallet(@PathVariable String palletUUID) {
        return palletFacade.deletePallet(palletUUID);
    }

    @GetMapping("/{palletUUID}/products")
    public List<ProductResponseDto> findAllPalletProducts(@PathVariable String palletUUID) {
        return palletFacade.findAllPalletProducts(palletUUID);
    }

    @PutMapping("/{pallet_uuid}/products/{removed_product_uuid}/{added_product_uuid}")
    public ProductResponseDto updateProductOnPallet(@PathVariable("pallet_uuid") String palletUUID, @PathVariable("removed_product_uuid") String removedProductUuid, @PathVariable("added_product_uuid") String addedProductUuid) {
        return palletFacade.updateProductOnPallet(palletUUID, removedProductUuid, addedProductUuid);
    }

    @PostMapping("/{pallet_uuid}/products/{product_uuid}")
    public ProductResponseDto addProductOnPallet(@PathVariable("pallet_uuid") String palletUUID, @PathVariable("product_uuid") String productUuid) {
        return palletFacade.addProductOnPallet(palletUUID, productUuid);
    }

    @DeleteMapping("/products/{product_uuid}")
    public ProductResponseDto deleteProductFromPallet(@PathVariable("product_uuid") String productUuId) {
        return palletFacade.deleteProductFromPallet(productUuId);
    }

    @DeleteMapping("/{pallet_uuid}/products")
    public List<ProductResponseDto> deleteAllFromPallet(@PathVariable("pallet_uuid") String palletUuid) {
        return palletFacade.deleteAllFromPallet(palletUuid);
    }
}