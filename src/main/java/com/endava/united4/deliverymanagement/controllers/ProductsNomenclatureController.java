package com.endava.united4.deliverymanagement.controllers;

import com.endava.united4.deliverymanagement.dto.ProductNomenclatureRequestDto;
import com.endava.united4.deliverymanagement.dto.ProductNomenclatureResponseDto;
import com.endava.united4.deliverymanagement.dto.ProductRequestDto;
import com.endava.united4.deliverymanagement.dto.ProductResponseDto;
import com.endava.united4.deliverymanagement.facade.NomenclatureFacade;
import com.endava.united4.deliverymanagement.facade.ProductFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/nomenclature")
@RequiredArgsConstructor
public class ProductsNomenclatureController {

    private final NomenclatureFacade nomenclatureFacade;
    private final ProductFacade productFacade;

    @PostMapping
    public ProductNomenclatureResponseDto saveNomenclature(@RequestBody ProductNomenclatureRequestDto productNomenclatureRequestDto) {
        return nomenclatureFacade.saveProduct(productNomenclatureRequestDto);
    }

    @GetMapping
    public List<ProductNomenclatureResponseDto> getAllNomenclatures() {
        return nomenclatureFacade.findAll();
    }

    @PutMapping("/{nomenclatureUuid}")
    public ProductNomenclatureResponseDto updateNomenclature(@RequestBody ProductNomenclatureRequestDto productNomenclatureRequestDto, @PathVariable String nomenclatureUuid) {
        return nomenclatureFacade.updateProduct(productNomenclatureRequestDto, nomenclatureUuid);
    }

    @DeleteMapping("/{nomenclatureUuid}")
    public ProductNomenclatureResponseDto deleteNomenclature(@PathVariable String nomenclatureUuid) {
        return nomenclatureFacade.deleteProduct(nomenclatureUuid);
    }

    @GetMapping("/{nomenclatureUuid}")
    public ProductNomenclatureResponseDto findByNomenclatureUuid(@PathVariable String nomenclatureUuid) {
        return nomenclatureFacade.findByProductUuid(nomenclatureUuid);
    }

    @PostMapping("/{nomenclatureUuid}/product")
    public ProductResponseDto createProduct(@PathVariable String nomenclatureUuid, @RequestBody ProductRequestDto productRequestDto) {
        return productFacade.createProduct(nomenclatureUuid, productRequestDto);
    }
}