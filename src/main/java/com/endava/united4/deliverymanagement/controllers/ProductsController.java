package com.endava.united4.deliverymanagement.controllers;

import com.endava.united4.deliverymanagement.dto.ProductRequestDto;
import com.endava.united4.deliverymanagement.dto.ProductResponseDto;
import com.endava.united4.deliverymanagement.facade.ProductFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductsController {

    private final ProductFacade productFacade;

    @GetMapping
    public List<ProductResponseDto> getAllProducts() {
        return productFacade.findAll();
    }

    @PutMapping("/{productUuid}")
    public ProductResponseDto updateProduct(@RequestBody ProductRequestDto product, @PathVariable String productUuid) {
        return productFacade.updateProduct(productUuid, product);
    }

    @DeleteMapping("/{productUuid}")
    public ProductResponseDto deleteProduct(@PathVariable String productUuid) {
        return productFacade.deleteProduct(productUuid);
    }

    @GetMapping("/{productUuid}")
    public ProductResponseDto findByProductByUuid(@PathVariable String productUuid) {
        return productFacade.findProductByUuid(productUuid);
    }
}
