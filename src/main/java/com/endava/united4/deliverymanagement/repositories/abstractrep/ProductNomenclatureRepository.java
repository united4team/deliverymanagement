package com.endava.united4.deliverymanagement.repositories.abstractrep;

import com.endava.united4.deliverymanagement.entities.ProductNomenclature;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductNomenclatureRepository extends JpaRepository<ProductNomenclature, String> {
}
