RENAME Products TO Product_nomenclature;
ALTER TABLE Product_nomenclature DROP COLUMN PRODUCTION_DATE;
ALTER TABLE Product_nomenclature MODIFY NOMENCLATURE_UUID UNIQUE;
ALTER TABLE Product_nomenclature DROP COLUMN pallet_uuid;

