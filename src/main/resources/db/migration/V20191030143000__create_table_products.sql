CREATE TABLE products
(
    product_id           NUMBER(6)        PRIMARY KEY,
    product_name         VARCHAR2(20),
    weight               NUMBER(8, 2)     NOT NULL,
    price                NUMBER(8, 2)     NOT NULL,
    exp_duration         number           NOT NULL,
    production_date      DATE             NOT NULL,
    product_ui_id        NUMBER(16)       NOT NULL
);

CREATE SEQUENCE products_seq NOCACHE;
ALTER TABLE products
    MODIFY product_id DEFAULT products_seq.nextval;

insert into products (product_name, weight, price, exp_duration, production_date, product_ui_id)values('Product1', 5.3, 100, 3, to_date('01-01-2013', 'dd-mm-yyyy'),1);
insert into products (product_name, weight, price, exp_duration, production_date, product_ui_id)values('Product2', 2, 99, 3, to_date('01-01-2013', 'dd-mm-yyyy'),2);
insert into products (product_name, weight, price, exp_duration, production_date, product_ui_id)values('Product3', 8, 75, 3, to_date('01-01-2013', 'dd-mm-yyyy'),3);
insert into products (product_name, weight, price, exp_duration, production_date, product_ui_id)values('Product4', 12.7, 13, 3, to_date('01-01-2013', 'dd-mm-yyyy'),4);
insert into products (product_name, weight, price, exp_duration, production_date, product_ui_id)values('Product5', 16.9, 1, 3, to_date('01-01-2013','dd-mm-yyyy'),5);




